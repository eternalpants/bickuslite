import net.runelite.mapping.Implements;
import net.runelite.mapping.ObfuscatedGetter;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("jy")
@Implements("MusicPatchNode2")
public class MusicPatchNode2 {
	@ObfuscatedName("v")
	byte[] field3160;
	@ObfuscatedName("c")
	byte[] field3165;
	@ObfuscatedName("i")
	@ObfuscatedGetter(
		intValue = -1091435735
	)
	int field3162;
	@ObfuscatedName("f")
	@ObfuscatedGetter(
		intValue = 1957731725
	)
	int field3159;
	@ObfuscatedName("b")
	@ObfuscatedGetter(
		intValue = 897940521
	)
	int field3163;
	@ObfuscatedName("n")
	@ObfuscatedGetter(
		intValue = 100002585
	)
	int field3164;
	@ObfuscatedName("s")
	@ObfuscatedGetter(
		intValue = 369050739
	)
	int field3161;
	@ObfuscatedName("l")
	@ObfuscatedGetter(
		intValue = 199056997
	)
	int field3166;
	@ObfuscatedName("q")
	@ObfuscatedGetter(
		intValue = 1008423979
	)
	int field3167;

	MusicPatchNode2() {
	} // L: 14
}
