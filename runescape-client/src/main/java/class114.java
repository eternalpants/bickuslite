import net.runelite.mapping.ObfuscatedName;
import net.runelite.mapping.ObfuscatedSignature;

@ObfuscatedName("dv")
public class class114 {
	@ObfuscatedName("f")
	public static final float field1382;
	@ObfuscatedName("b")
	public static final float field1380;

	static {
		field1382 = Math.ulp(1.0F); // L: 10
		field1380 = 2.0F * field1382; // L: 11
	}

	@ObfuscatedName("v")
	@ObfuscatedSignature(
		descriptor = "(I)[Ljp;",
		garbageValue = "-1344733039"
	)
	public static class263[] method2662() {
		return new class263[]{class263.field3006, class263.field3005, class263.field3015, class263.field3007, class263.field3008, class263.field3009, class263.field3004, class263.field3011, class263.field3012, class263.field3013}; // L: 17
	}
}
