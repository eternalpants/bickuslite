import net.runelite.mapping.Export;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("mm")
public class class357 {
	@ObfuscatedName("w")
	@Export("Tiles_hueMultiplier")
	static int[] Tiles_hueMultiplier;
}
