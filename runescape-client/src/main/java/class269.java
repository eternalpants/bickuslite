import net.runelite.mapping.ObfuscatedGetter;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("jz")
public final class class269 {
	@ObfuscatedName("v")
	@ObfuscatedGetter(
		longValue = 99828989158930681L
	)
	static long field3149;
	@ObfuscatedName("c")
	@ObfuscatedGetter(
		longValue = -6438905473157114757L
	)
	static long field3147;
}
