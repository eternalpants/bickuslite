import net.runelite.mapping.Export;
import net.runelite.mapping.ObfuscatedName;
import net.runelite.mapping.ObfuscatedSignature;

@ObfuscatedName("oh")
public class class393 extends class394 {
	@ObfuscatedName("cc")
	@ObfuscatedSignature(
		descriptor = "[Lqi;"
	)
	@Export("worldSelectBackSprites")
	static SpritePixels[] worldSelectBackSprites;

	public class393(int var1) {
		super(var1); // L: 7
	} // L: 8

	@ObfuscatedName("v")
	@ObfuscatedSignature(
		descriptor = "(Lpi;IS)V",
		garbageValue = "256"
	)
	void vmethod7129(Buffer var1, int var2) {
	} // L: 11
}
