import net.runelite.mapping.Export;
import net.runelite.mapping.ObfuscatedName;
import net.runelite.mapping.ObfuscatedSignature;

@ObfuscatedName("ai")
public interface class45 {
	@ObfuscatedName("v")
	@ObfuscatedSignature(
		descriptor = "(I)Lay;",
		garbageValue = "-262480710"
	)
	@Export("player")
	PcmPlayer player();
}
