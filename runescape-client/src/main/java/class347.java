import java.security.SecureRandom;
import net.runelite.mapping.Export;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("mt")
public final class class347 {
	@ObfuscatedName("fh")
	@Export("secureRandom")
	static SecureRandom secureRandom;
}
