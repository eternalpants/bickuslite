import net.runelite.mapping.Export;
import net.runelite.mapping.Implements;
import net.runelite.mapping.ObfuscatedGetter;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ip")
@Implements("Occluder")
public final class Occluder {
	@ObfuscatedName("bs")
	@ObfuscatedGetter(
		intValue = 521485803
	)
	static int field2687;
	@ObfuscatedName("v")
	@ObfuscatedGetter(
		intValue = 432924175
	)
	@Export("minTileX")
	int minTileX;
	@ObfuscatedName("c")
	@ObfuscatedGetter(
		intValue = 870286189
	)
	@Export("maxTileX")
	int maxTileX;
	@ObfuscatedName("i")
	@ObfuscatedGetter(
		intValue = -1384680613
	)
	@Export("minTileY")
	int minTileY;
	@ObfuscatedName("f")
	@ObfuscatedGetter(
		intValue = 307229679
	)
	@Export("maxTileY")
	int maxTileY;
	@ObfuscatedName("b")
	@ObfuscatedGetter(
		intValue = -2026987549
	)
	@Export("type")
	int type;
	@ObfuscatedName("n")
	@ObfuscatedGetter(
		intValue = -1038398405
	)
	@Export("minX")
	int minX;
	@ObfuscatedName("s")
	@ObfuscatedGetter(
		intValue = -1926841753
	)
	@Export("maxX")
	int maxX;
	@ObfuscatedName("l")
	@ObfuscatedGetter(
		intValue = 933387719
	)
	@Export("minZ")
	int minZ;
	@ObfuscatedName("q")
	@ObfuscatedGetter(
		intValue = 1713258203
	)
	@Export("maxZ")
	int maxZ;
	@ObfuscatedName("o")
	@ObfuscatedGetter(
		intValue = 293086529
	)
	@Export("minY")
	int minY;
	@ObfuscatedName("r")
	@ObfuscatedGetter(
		intValue = -324431291
	)
	@Export("maxY")
	int maxY;
	@ObfuscatedName("p")
	@ObfuscatedGetter(
		intValue = -1076390299
	)
	int field2669;
	@ObfuscatedName("w")
	@ObfuscatedGetter(
		intValue = -1300048983
	)
	int field2681;
	@ObfuscatedName("k")
	@ObfuscatedGetter(
		intValue = 1268857779
	)
	int field2682;
	@ObfuscatedName("d")
	@ObfuscatedGetter(
		intValue = 156478241
	)
	int field2683;
	@ObfuscatedName("m")
	@ObfuscatedGetter(
		intValue = -213765683
	)
	int field2684;
	@ObfuscatedName("u")
	@ObfuscatedGetter(
		intValue = -606308779
	)
	int field2685;
	@ObfuscatedName("t")
	@ObfuscatedGetter(
		intValue = 1936535027
	)
	int field2686;

	Occluder() {
	} // L: 23
}
