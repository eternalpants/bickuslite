import net.runelite.mapping.ObfuscatedName;
import net.runelite.mapping.ObfuscatedSignature;

@ObfuscatedName("f")
public interface class3 {
	@ObfuscatedName("v")
	@ObfuscatedSignature(
		descriptor = "(Lpi;)Lpi;"
	)
	Buffer vmethod12(Buffer var1);
}
