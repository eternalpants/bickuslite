import net.runelite.mapping.ObfuscatedGetter;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("mk")
public final class class340 {
	@ObfuscatedName("v")
	final Object field4096;
	@ObfuscatedName("c")
	@ObfuscatedGetter(
		intValue = 1768359201
	)
	int field4095;

	class340(Object var1, int var2) {
		this.field4096 = var1; // L: 158
		this.field4095 = var2; // L: 159
	} // L: 160
}
