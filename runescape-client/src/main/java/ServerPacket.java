import net.runelite.mapping.Export;
import net.runelite.mapping.Implements;
import net.runelite.mapping.ObfuscatedGetter;
import net.runelite.mapping.ObfuscatedName;
import net.runelite.mapping.ObfuscatedSignature;

@ObfuscatedName("jq")
@Implements("ServerPacket")
public class ServerPacket {
	@ObfuscatedName("v")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3033;
	@ObfuscatedName("c")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3053;
	@ObfuscatedName("i")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3096;
	@ObfuscatedName("f")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3026;
	@ObfuscatedName("b")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3038;
	@ObfuscatedName("n")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3028;
	@ObfuscatedName("s")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3025;
	@ObfuscatedName("l")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3030;
	@ObfuscatedName("q")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3031;
	@ObfuscatedName("o")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3027;
	@ObfuscatedName("r")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3072;
	@ObfuscatedName("p")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3116;
	@ObfuscatedName("w")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3035;
	@ObfuscatedName("k")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3036;
	@ObfuscatedName("d")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3037;
	@ObfuscatedName("m")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3128;
	@ObfuscatedName("u")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3114;
	@ObfuscatedName("t")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3111;
	@ObfuscatedName("g")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3041;
	@ObfuscatedName("x")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3042;
	@ObfuscatedName("a")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3097;
	@ObfuscatedName("y")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3044;
	@ObfuscatedName("j")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3086;
	@ObfuscatedName("e")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3047;
	@ObfuscatedName("z")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3119;
	@ObfuscatedName("h")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3048;
	@ObfuscatedName("ae")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3049;
	@ObfuscatedName("aq")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3050;
	@ObfuscatedName("aw")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3034;
	@ObfuscatedName("am")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3052;
	@ObfuscatedName("ak")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3029;
	@ObfuscatedName("ao")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3023;
	@ObfuscatedName("aj")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3024;
	@ObfuscatedName("al")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3056;
	@ObfuscatedName("av")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3057;
	@ObfuscatedName("at")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3058;
	@ObfuscatedName("an")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3045;
	@ObfuscatedName("ay")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3060;
	@ObfuscatedName("ag")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3054;
	@ObfuscatedName("ah")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3062;
	@ObfuscatedName("ac")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3063;
	@ObfuscatedName("ab")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3064;
	@ObfuscatedName("au")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3101;
	@ObfuscatedName("af")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3066;
	@ObfuscatedName("ad")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3067;
	@ObfuscatedName("ai")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3068;
	@ObfuscatedName("ax")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3105;
	@ObfuscatedName("ar")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3070;
	@ObfuscatedName("ap")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3071;
	@ObfuscatedName("az")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3120;
	@ObfuscatedName("as")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3073;
	@ObfuscatedName("aa")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3079;
	@ObfuscatedName("bj")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3043;
	@ObfuscatedName("bm")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3076;
	@ObfuscatedName("br")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3077;
	@ObfuscatedName("bo")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3078;
	@ObfuscatedName("bl")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3069;
	@ObfuscatedName("be")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3080;
	@ObfuscatedName("bh")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3081;
	@ObfuscatedName("bf")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3115;
	@ObfuscatedName("bb")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3083;
	@ObfuscatedName("bw")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3084;
	@ObfuscatedName("bx")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3085;
	@ObfuscatedName("bg")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3061;
	@ObfuscatedName("bn")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3087;
	@ObfuscatedName("bq")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3046;
	@ObfuscatedName("ba")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3089;
	@ObfuscatedName("bk")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3090;
	@ObfuscatedName("bs")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3091;
	@ObfuscatedName("by")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3092;
	@ObfuscatedName("bt")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3093;
	@ObfuscatedName("bd")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3094;
	@ObfuscatedName("bc")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3051;
	@ObfuscatedName("bp")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3095;
	@ObfuscatedName("bi")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3032;
	@ObfuscatedName("bu")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3040;
	@ObfuscatedName("bz")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3099;
	@ObfuscatedName("bv")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3100;
	@ObfuscatedName("cm")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3074;
	@ObfuscatedName("cc")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3102;
	@ObfuscatedName("ch")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3088;
	@ObfuscatedName("cf")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3103;
	@ObfuscatedName("cv")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3075;
	@ObfuscatedName("cs")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3106;
	@ObfuscatedName("cz")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3107;
	@ObfuscatedName("ct")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3108;
	@ObfuscatedName("cb")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3109;
	@ObfuscatedName("cp")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3110;
	@ObfuscatedName("co")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3055;
	@ObfuscatedName("ca")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3112;
	@ObfuscatedName("ci")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3113;
	@ObfuscatedName("cy")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3039;
	@ObfuscatedName("cr")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3059;
	@ObfuscatedName("cq")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3104;
	@ObfuscatedName("cn")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3117;
	@ObfuscatedName("cu")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3118;
	@ObfuscatedName("cg")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3082;
	@ObfuscatedName("cj")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3098;
	@ObfuscatedName("ce")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3121;
	@ObfuscatedName("cl")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3122;
	@ObfuscatedName("ck")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3123;
	@ObfuscatedName("cd")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3124;
	@ObfuscatedName("cw")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3125;
	@ObfuscatedName("cx")
	@ObfuscatedSignature(
		descriptor = "Ljq;"
	)
	public static final ServerPacket field3126;
	@ObfuscatedName("ge")
	@ObfuscatedGetter(
		longValue = -6208068858010154241L
	)
	static long field3129;
	@ObfuscatedName("dr")
	@ObfuscatedGetter(
		intValue = 766304815
	)
	@Export("id")
	public final int id;
	@ObfuscatedName("dj")
	@ObfuscatedGetter(
		intValue = -1617993979
	)
	@Export("length")
	public final int length;

	static {
		field3033 = new ServerPacket(0, -2); // L: 5
		field3053 = new ServerPacket(1, -2); // L: 6
		field3096 = new ServerPacket(2, 4); // L: 7
		field3026 = new ServerPacket(3, -2); // L: 8
		field3038 = new ServerPacket(4, 2); // L: 9
		field3028 = new ServerPacket(5, 1); // L: 10
		field3025 = new ServerPacket(6, 1); // L: 11
		field3030 = new ServerPacket(7, -2); // L: 12
		field3031 = new ServerPacket(8, 6); // L: 13
		field3027 = new ServerPacket(9, 1); // L: 14
		field3072 = new ServerPacket(10, 3); // L: 15
		field3116 = new ServerPacket(11, 4); // L: 16
		field3035 = new ServerPacket(12, -2); // L: 17
		field3036 = new ServerPacket(13, 2); // L: 18
		field3037 = new ServerPacket(14, -2); // L: 19
		field3128 = new ServerPacket(15, 6); // L: 20
		field3114 = new ServerPacket(16, 2); // L: 21
		field3111 = new ServerPacket(17, -1); // L: 22
		field3041 = new ServerPacket(18, -2); // L: 23
		field3042 = new ServerPacket(19, -2); // L: 24
		field3097 = new ServerPacket(20, 8); // L: 25
		field3044 = new ServerPacket(21, -2); // L: 26
		field3086 = new ServerPacket(22, 4); // L: 27
		field3047 = new ServerPacket(23, 10); // L: 28
		field3119 = new ServerPacket(24, 2); // L: 29
		field3048 = new ServerPacket(25, 2); // L: 30
		field3049 = new ServerPacket(26, -1); // L: 31
		field3050 = new ServerPacket(27, 1); // L: 32
		field3034 = new ServerPacket(28, 2); // L: 33
		field3052 = new ServerPacket(29, 6); // L: 34
		field3029 = new ServerPacket(30, -2); // L: 35
		field3023 = new ServerPacket(31, -2); // L: 36
		field3024 = new ServerPacket(32, 8); // L: 37
		field3056 = new ServerPacket(33, 0); // L: 38
		field3057 = new ServerPacket(34, -1); // L: 39
		field3058 = new ServerPacket(35, -2); // L: 40
		field3045 = new ServerPacket(36, 5); // L: 41
		field3060 = new ServerPacket(37, -2); // L: 42
		field3054 = new ServerPacket(38, 10); // L: 43
		field3062 = new ServerPacket(39, -1); // L: 44
		field3063 = new ServerPacket(40, 14); // L: 45
		field3064 = new ServerPacket(41, 5); // L: 46
		field3101 = new ServerPacket(42, 1); // L: 47
		field3066 = new ServerPacket(43, 3); // L: 48
		field3067 = new ServerPacket(44, -1); // L: 49
		field3068 = new ServerPacket(45, -2); // L: 50
		field3105 = new ServerPacket(46, 0); // L: 51
		field3070 = new ServerPacket(47, 12); // L: 52
		field3071 = new ServerPacket(48, 8); // L: 53
		field3120 = new ServerPacket(49, 0); // L: 54
		field3073 = new ServerPacket(50, 0); // L: 55
		field3079 = new ServerPacket(51, 2); // L: 56
		field3043 = new ServerPacket(52, -2); // L: 57
		field3076 = new ServerPacket(53, 20); // L: 58
		field3077 = new ServerPacket(54, 4); // L: 59
		field3078 = new ServerPacket(55, 6); // L: 60
		field3069 = new ServerPacket(56, -2); // L: 61
		field3080 = new ServerPacket(57, 0); // L: 62
		field3081 = new ServerPacket(58, 1); // L: 63
		field3115 = new ServerPacket(59, 2); // L: 64
		field3083 = new ServerPacket(60, -1); // L: 65
		field3084 = new ServerPacket(61, 28); // L: 66
		field3085 = new ServerPacket(62, 0); // L: 67
		field3061 = new ServerPacket(63, -2); // L: 68
		field3087 = new ServerPacket(64, -2); // L: 69
		field3046 = new ServerPacket(65, 5); // L: 70
		field3089 = new ServerPacket(66, -1); // L: 71
		field3090 = new ServerPacket(67, 15); // L: 72
		field3091 = new ServerPacket(68, 0); // L: 73
		field3092 = new ServerPacket(69, -2); // L: 74
		field3093 = new ServerPacket(70, 6); // L: 75
		field3094 = new ServerPacket(71, 0); // L: 76
		field3051 = new ServerPacket(72, 0); // L: 77
		field3095 = new ServerPacket(73, 4); // L: 78
		field3032 = new ServerPacket(74, 4); // L: 79
		field3040 = new ServerPacket(75, 4); // L: 80
		field3099 = new ServerPacket(76, -2); // L: 81
		field3100 = new ServerPacket(77, 5); // L: 82
		field3074 = new ServerPacket(78, -2); // L: 83
		field3102 = new ServerPacket(79, 8); // L: 84
		field3088 = new ServerPacket(80, 6); // L: 85
		field3103 = new ServerPacket(81, 7); // L: 86
		field3075 = new ServerPacket(82, 2); // L: 87
		field3106 = new ServerPacket(83, 5); // L: 88
		field3107 = new ServerPacket(84, -2); // L: 89
		field3108 = new ServerPacket(85, 7); // L: 90
		field3109 = new ServerPacket(86, -2); // L: 91
		field3110 = new ServerPacket(87, 6); // L: 92
		field3055 = new ServerPacket(88, 2); // L: 93
		field3112 = new ServerPacket(89, -1); // L: 94
		field3113 = new ServerPacket(90, -2); // L: 95
		field3039 = new ServerPacket(91, -1); // L: 96
		field3059 = new ServerPacket(92, 4); // L: 97
		field3104 = new ServerPacket(93, 6); // L: 98
		field3117 = new ServerPacket(94, 6); // L: 99
		field3118 = new ServerPacket(95, 6); // L: 100
		field3082 = new ServerPacket(96, -2); // L: 101
		field3098 = new ServerPacket(97, 6); // L: 102
		field3121 = new ServerPacket(98, 5); // L: 103
		field3122 = new ServerPacket(99, 6); // L: 104
		field3123 = new ServerPacket(100, 8); // L: 105
		field3124 = new ServerPacket(101, 8); // L: 106
		field3125 = new ServerPacket(102, 8); // L: 107
		field3126 = new ServerPacket(103, 17); // L: 108
	}

	ServerPacket(int var1, int var2) {
		this.id = var1; // L: 117
		this.length = var2; // L: 118
	} // L: 119
}
