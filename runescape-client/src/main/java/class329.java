import java.util.HashMap;
import net.runelite.mapping.Export;
import net.runelite.mapping.ObfuscatedGetter;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("lm")
public class class329 {
	@ObfuscatedName("fa")
	@ObfuscatedGetter(
		intValue = 1869767523
	)
	@Export("currentPort")
	static int currentPort;

	static {
		new HashMap();
	} // L: 9
}
