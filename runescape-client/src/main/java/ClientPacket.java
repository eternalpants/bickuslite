import net.runelite.mapping.Export;
import net.runelite.mapping.Implements;
import net.runelite.mapping.ObfuscatedGetter;
import net.runelite.mapping.ObfuscatedName;
import net.runelite.mapping.ObfuscatedSignature;

@ObfuscatedName("jl")
@Implements("ClientPacket")
public class ClientPacket implements class261 {
	@ObfuscatedName("ro")
	@ObfuscatedSignature(
		descriptor = "Lns;"
	)
	@Export("friendsChat")
	static FriendsChat friendsChat;
	@ObfuscatedName("v")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2922;
	@ObfuscatedName("c")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2977;
	@ObfuscatedName("i")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2929;
	@ObfuscatedName("f")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2896;
	@ObfuscatedName("b")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	static final ClientPacket field2934;
	@ObfuscatedName("n")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2893;
	@ObfuscatedName("s")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2899;
	@ObfuscatedName("l")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2955;
	@ObfuscatedName("q")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2901;
	@ObfuscatedName("o")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2902;
	@ObfuscatedName("r")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2931;
	@ObfuscatedName("p")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2907;
	@ObfuscatedName("w")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2905;
	@ObfuscatedName("k")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2906;
	@ObfuscatedName("d")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2916;
	@ObfuscatedName("m")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2908;
	@ObfuscatedName("u")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2958;
	@ObfuscatedName("t")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2910;
	@ObfuscatedName("g")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2911;
	@ObfuscatedName("x")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2912;
	@ObfuscatedName("a")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2913;
	@ObfuscatedName("y")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2914;
	@ObfuscatedName("j")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2915;
	@ObfuscatedName("e")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2924;
	@ObfuscatedName("z")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2917;
	@ObfuscatedName("h")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2997;
	@ObfuscatedName("ae")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2919;
	@ObfuscatedName("aq")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2974;
	@ObfuscatedName("aw")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2909;
	@ObfuscatedName("am")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2920;
	@ObfuscatedName("ak")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2923;
	@ObfuscatedName("ao")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2904;
	@ObfuscatedName("aj")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2925;
	@ObfuscatedName("al")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field3000;
	@ObfuscatedName("av")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2927;
	@ObfuscatedName("at")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2918;
	@ObfuscatedName("an")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2898;
	@ObfuscatedName("ay")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2930;
	@ObfuscatedName("ag")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2928;
	@ObfuscatedName("ah")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2894;
	@ObfuscatedName("ac")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2933;
	@ObfuscatedName("ab")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2993;
	@ObfuscatedName("au")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2935;
	@ObfuscatedName("af")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2980;
	@ObfuscatedName("ad")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2897;
	@ObfuscatedName("ai")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2938;
	@ObfuscatedName("ax")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2939;
	@ObfuscatedName("ar")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2940;
	@ObfuscatedName("ap")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2941;
	@ObfuscatedName("az")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2942;
	@ObfuscatedName("as")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2943;
	@ObfuscatedName("aa")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2944;
	@ObfuscatedName("bj")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2945;
	@ObfuscatedName("bm")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	static final ClientPacket field2946;
	@ObfuscatedName("br")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2995;
	@ObfuscatedName("bo")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2954;
	@ObfuscatedName("bl")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2949;
	@ObfuscatedName("be")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2950;
	@ObfuscatedName("bh")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2951;
	@ObfuscatedName("bf")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2900;
	@ObfuscatedName("bb")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2953;
	@ObfuscatedName("bw")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	static final ClientPacket field2947;
	@ObfuscatedName("bx")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2976;
	@ObfuscatedName("bg")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2992;
	@ObfuscatedName("bn")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2957;
	@ObfuscatedName("bq")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2952;
	@ObfuscatedName("ba")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2959;
	@ObfuscatedName("bk")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2960;
	@ObfuscatedName("bs")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2961;
	@ObfuscatedName("by")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2962;
	@ObfuscatedName("bt")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2963;
	@ObfuscatedName("bd")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2964;
	@ObfuscatedName("bc")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2965;
	@ObfuscatedName("bp")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2966;
	@ObfuscatedName("bi")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2967;
	@ObfuscatedName("bu")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	static final ClientPacket field2968;
	@ObfuscatedName("bz")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2969;
	@ObfuscatedName("bv")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2970;
	@ObfuscatedName("cm")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2971;
	@ObfuscatedName("cc")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2972;
	@ObfuscatedName("ch")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2973;
	@ObfuscatedName("cf")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2926;
	@ObfuscatedName("cv")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2990;
	@ObfuscatedName("cs")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2999;
	@ObfuscatedName("cz")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2895;
	@ObfuscatedName("ct")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2978;
	@ObfuscatedName("cb")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2979;
	@ObfuscatedName("cp")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2937;
	@ObfuscatedName("co")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2981;
	@ObfuscatedName("ca")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2982;
	@ObfuscatedName("ci")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2983;
	@ObfuscatedName("cy")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2984;
	@ObfuscatedName("cr")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2956;
	@ObfuscatedName("cq")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2986;
	@ObfuscatedName("cn")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2987;
	@ObfuscatedName("cu")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2988;
	@ObfuscatedName("cg")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2989;
	@ObfuscatedName("cj")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	static final ClientPacket field2903;
	@ObfuscatedName("ce")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2991;
	@ObfuscatedName("cl")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2948;
	@ObfuscatedName("ck")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2936;
	@ObfuscatedName("cd")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2994;
	@ObfuscatedName("cw")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2921;
	@ObfuscatedName("cx")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2996;
	@ObfuscatedName("dr")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2985;
	@ObfuscatedName("dj")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2998;
	@ObfuscatedName("dd")
	@ObfuscatedSignature(
		descriptor = "Ljl;"
	)
	public static final ClientPacket field2975;
	@ObfuscatedName("jw")
	@ObfuscatedGetter(
		intValue = -2097783973
	)
	@Export("oculusOrbFocalPointY")
	static int oculusOrbFocalPointY;
	@ObfuscatedName("dg")
	@ObfuscatedGetter(
		intValue = -31948183
	)
	@Export("id")
	final int id;
	@ObfuscatedName("de")
	@ObfuscatedGetter(
		intValue = 824174823
	)
	@Export("length")
	final int length;

	static {
		field2922 = new ClientPacket(0, 8); // L: 5
		field2977 = new ClientPacket(1, 15); // L: 6
		field2929 = new ClientPacket(2, 9); // L: 7
		field2896 = new ClientPacket(3, 8); // L: 8
		field2934 = new ClientPacket(4, 7); // L: 9
		field2893 = new ClientPacket(5, 4); // L: 10
		field2899 = new ClientPacket(6, -2); // L: 11
		field2955 = new ClientPacket(7, 15); // L: 12
		field2901 = new ClientPacket(8, 7); // L: 13
		field2902 = new ClientPacket(9, 3); // L: 14
		field2931 = new ClientPacket(10, 3); // L: 15
		field2907 = new ClientPacket(11, -2); // L: 16
		field2905 = new ClientPacket(12, 0); // L: 17
		field2906 = new ClientPacket(13, 8); // L: 18
		field2916 = new ClientPacket(14, 3); // L: 19
		field2908 = new ClientPacket(15, 8); // L: 20
		field2958 = new ClientPacket(16, 8); // L: 21
		field2910 = new ClientPacket(17, -1); // L: 22
		field2911 = new ClientPacket(18, -1); // L: 23
		field2912 = new ClientPacket(19, -1); // L: 24
		field2913 = new ClientPacket(20, 3); // L: 25
		field2914 = new ClientPacket(21, 7); // L: 26
		field2915 = new ClientPacket(22, 7); // L: 27
		field2924 = new ClientPacket(23, 3); // L: 28
		field2917 = new ClientPacket(24, 8); // L: 29
		field2997 = new ClientPacket(25, 8); // L: 30
		field2919 = new ClientPacket(26, 16); // L: 31
		field2974 = new ClientPacket(27, 3); // L: 32
		field2909 = new ClientPacket(28, -1); // L: 33
		field2920 = new ClientPacket(29, 7); // L: 34
		field2923 = new ClientPacket(30, 3); // L: 35
		field2904 = new ClientPacket(31, 15); // L: 36
		field2925 = new ClientPacket(32, 3); // L: 37
		field3000 = new ClientPacket(33, 5); // L: 38
		field2927 = new ClientPacket(34, 7); // L: 39
		field2918 = new ClientPacket(35, -1); // L: 40
		field2898 = new ClientPacket(36, -1); // L: 41
		field2930 = new ClientPacket(37, 11); // L: 42
		field2928 = new ClientPacket(38, -1); // L: 43
		field2894 = new ClientPacket(39, 16); // L: 44
		field2933 = new ClientPacket(40, 7); // L: 45
		field2993 = new ClientPacket(41, 7); // L: 46
		field2935 = new ClientPacket(42, -1); // L: 47
		field2980 = new ClientPacket(43, -1); // L: 48
		field2897 = new ClientPacket(44, 4); // L: 49
		field2938 = new ClientPacket(45, 8); // L: 50
		field2939 = new ClientPacket(46, 3); // L: 51
		field2940 = new ClientPacket(47, 3); // L: 52
		field2941 = new ClientPacket(48, 8); // L: 53
		field2942 = new ClientPacket(49, 7); // L: 54
		field2943 = new ClientPacket(50, 8); // L: 55
		field2944 = new ClientPacket(51, 8); // L: 56
		field2945 = new ClientPacket(52, 8); // L: 57
		field2946 = new ClientPacket(53, -1); // L: 58
		field2995 = new ClientPacket(54, 11); // L: 59
		field2954 = new ClientPacket(55, 8); // L: 60
		field2949 = new ClientPacket(56, 2); // L: 61
		field2950 = new ClientPacket(57, 0); // L: 62
		field2951 = new ClientPacket(58, 7);
		field2900 = new ClientPacket(59, -1);
		field2953 = new ClientPacket(60, -1); // L: 65
		field2947 = new ClientPacket(61, -1); // L: 66
		field2976 = new ClientPacket(62, 6); // L: 67
		field2992 = new ClientPacket(63, -1); // L: 68
		field2957 = new ClientPacket(64, 3); // L: 69
		field2952 = new ClientPacket(65, 6); // L: 70
		field2959 = new ClientPacket(66, 14); // L: 71
		field2960 = new ClientPacket(67, 8); // L: 72
		field2961 = new ClientPacket(68, -1); // L: 73
		field2962 = new ClientPacket(69, 2); // L: 74
		field2963 = new ClientPacket(70, -1); // L: 75
		field2964 = new ClientPacket(71, 3); // L: 76
		field2965 = new ClientPacket(72, -1); // L: 77
		field2966 = new ClientPacket(73, 8); // L: 78
		field2967 = new ClientPacket(74, -1); // L: 79
		field2968 = new ClientPacket(75, -1); // L: 80
		field2969 = new ClientPacket(76, 8); // L: 81
		field2970 = new ClientPacket(77, -1); // L: 82
		field2971 = new ClientPacket(78, 16); // L: 83
		field2972 = new ClientPacket(79, 3); // L: 84
		field2973 = new ClientPacket(80, 0); // L: 85
		field2926 = new ClientPacket(81, -2); // L: 86
		field2990 = new ClientPacket(82, 4); // L: 87
		field2999 = new ClientPacket(83, 11); // L: 88
		field2895 = new ClientPacket(84, 7); // L: 89
		field2978 = new ClientPacket(85, 1); // L: 90
		field2979 = new ClientPacket(86, 0); // L: 91
		field2937 = new ClientPacket(87, 4); // L: 92
		field2981 = new ClientPacket(88, 3); // L: 93
		field2982 = new ClientPacket(89, 8); // L: 94
		field2983 = new ClientPacket(90, 10); // L: 95
		field2984 = new ClientPacket(91, 13); // L: 96
		field2956 = new ClientPacket(92, 11); // L: 97
		field2986 = new ClientPacket(93, -1); // L: 98
		field2987 = new ClientPacket(94, -1); // L: 99
		field2988 = new ClientPacket(95, 2); // L: 100
		field2989 = new ClientPacket(96, 0); // L: 101
		field2903 = new ClientPacket(97, 2); // L: 102
		field2991 = new ClientPacket(98, -1); // L: 103
		field2948 = new ClientPacket(99, 4); // L: 104
		field2936 = new ClientPacket(100, 2); // L: 105
		field2994 = new ClientPacket(101, 15); // L: 106
		field2921 = new ClientPacket(102, 8); // L: 107
		field2996 = new ClientPacket(103, 9); // L: 108
		field2985 = new ClientPacket(104, 8); // L: 109
		field2998 = new ClientPacket(105, 8); // L: 110
		field2975 = new ClientPacket(106, 22); // L: 111
	}

	ClientPacket(int var1, int var2) {
		this.id = var1; // L: 116
		this.length = var2; // L: 117
	} // L: 118

	@ObfuscatedName("f")
	@ObfuscatedSignature(
		descriptor = "(II)V",
		garbageValue = "-1708925611"
	)
	public static void method5176(int var0) {
		MouseHandler.MouseHandler_idleCycles = var0; // L: 63
	} // L: 64

	@ObfuscatedName("ac")
	@ObfuscatedSignature(
		descriptor = "(II)I",
		garbageValue = "296193861"
	)
	static int method5175(int var0) {
		return (int)((Math.log((double)var0) / Interpreter.field845 - 7.0D) * 256.0D); // L: 3841
	}
}
