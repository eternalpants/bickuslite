import net.runelite.mapping.ObfuscatedName;
import net.runelite.mapping.ObfuscatedSignature;

@ObfuscatedName("pv")
public interface class440 {
	@ObfuscatedName("v")
	@ObfuscatedSignature(
		descriptor = "(Lpi;S)V"
	)
	void method7924(Buffer var1);
}
