import net.runelite.mapping.Export;
import net.runelite.mapping.ObfuscatedName;
import net.runelite.mapping.ObfuscatedSignature;

@ObfuscatedName("ht")
public class class216 {
	@ObfuscatedName("t")
	@Export("cacheSubPaths")
	static String[] cacheSubPaths;

	@ObfuscatedName("v")
	@ObfuscatedSignature(
		descriptor = "(I)[Lkm;",
		garbageValue = "620331303"
	)
	static class302[] method4365() {
		return new class302[]{class302.field3551, class302.field3549}; // L: 35
	}
}
