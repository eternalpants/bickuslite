import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("pg")
public class class422 {
	@ObfuscatedName("i")
	static final char[] field4560;
	@ObfuscatedName("f")
	static final char[] field4563;

	static {
		field4560 = new char[]{' ', ' ', '_', '-', 'à', 'á', 'â', 'ä', 'ã', 'À', 'Á', 'Â', 'Ä', 'Ã', 'è', 'é', 'ê', 'ë', 'È', 'É', 'Ê', 'Ë', 'í', 'î', 'ï', 'Í', 'Î', 'Ï', 'ò', 'ó', 'ô', 'ö', 'õ', 'Ò', 'Ó', 'Ô', 'Ö', 'Õ', 'ù', 'ú', 'û', 'ü', 'Ù', 'Ú', 'Û', 'Ü', 'ç', 'Ç', 'ÿ', 'Ÿ', 'ñ', 'Ñ', 'ß'}; // L: 8
		field4563 = new char[]{'[', ']', '#'}; // L: 9
	}
}
