import net.runelite.mapping.ObfuscatedGetter;
import net.runelite.mapping.ObfuscatedName;
import net.runelite.mapping.ObfuscatedSignature;

@ObfuscatedName("mx")
public class class359 {
	@ObfuscatedName("v")
	@ObfuscatedSignature(
		descriptor = "Lmx;"
	)
	public static final class359 field4204;
	@ObfuscatedName("c")
	@ObfuscatedSignature(
		descriptor = "Lmx;"
	)
	static final class359 field4205;
	@ObfuscatedName("i")
	@ObfuscatedGetter(
		intValue = -916709349
	)
	final int field4206;

	static {
		field4204 = new class359(1); // L: 5
		field4205 = new class359(0); // L: 6
	}

	class359(int var1) {
		this.field4206 = var1; // L: 10
	} // L: 11
}
